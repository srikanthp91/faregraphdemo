package com.srikanth.graphfarecalender;

/**
 * Created by srikanth on 8/5/18.
 */


public class CoOrdinatePojo {
    private float xCoOrdinate;
    private float yCoOrdinate;

    public CoOrdinatePojo(float xCoOrdinate, float yCoOrdinate) {
        this.xCoOrdinate = xCoOrdinate;
        this.yCoOrdinate = yCoOrdinate;
    }

    public float getXCoOrdinate() {
        return xCoOrdinate;
    }

    public float getYCoOrdinate() {
        return yCoOrdinate;
    }
}
