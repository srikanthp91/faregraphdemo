package com.srikanth.graphfarecalender;

/**
 * Created by srikanth on 7/5/18.
 */

public class FareCalItem {
    int fareValue;
    String date;
    String day;

    public FareCalItem(int fareValue, String day, String date) {
        this.fareValue = fareValue;
        this.date = date;
        this.day = day;
    }

    public int getFareValue() {
        return fareValue;
    }

    public String getDate() {
        return date;
    }

    public String getDay() {
        return day;
    }
}
