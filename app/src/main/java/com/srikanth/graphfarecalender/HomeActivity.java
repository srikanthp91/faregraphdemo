package com.srikanth.graphfarecalender;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private static final int minValue = 3000;
    private static final int maxValue = 4000;
    private static final int CIRCLE_IMAGE_WIDTH = 30;
    private int mCurrentSelectedDatePosition = 0;
    private int mPreviousSelectedDatePosition = 0;
    int textViewId = 100;

    private List<FareCalItem> mFareCalItems = new ArrayList<>();
    private List<CoOrdinatePojo> mCircleCoordinates = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        prepareFareCalData();
        createViewsAndAddToScreen();

    }


    private void createViewsAndAddToScreen() {


        for (int i = 0; i < mFareCalItems.size(); i++) {
            FareCalItem fareCalItem = mFareCalItems.get(i);
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View fareCalView = layoutInflater.inflate(R.layout.fare_cal_graph_item, null);
            ((TextView) fareCalView.findViewById(R.id.txt_fare_cal_day_value)).setText(fareCalItem.getDay());
            ((TextView) fareCalView.findViewById(R.id.txt_fare_cal_date_value)).setText(fareCalItem.getDate());

            final int percentageOfIncreaseInFare =
                    getPercentageOfIncreaseInFare(minValue, maxValue, fareCalItem.getFareValue());
            setCircularImageOnLine(fareCalItem, 100 - percentageOfIncreaseInFare, fareCalView, i);
            //

            //Add view to parent
            ViewGroup parentLinearLyt = (ViewGroup) findViewById(R.id.lyt_parent_linear);
            parentLinearLyt.addView(fareCalView);
        }
    }


    private void setCircularImageOnLine(final FareCalItem fareCalItem, final int percentageOfIncreaseInFare, final View parentView, final int childPosition) {
        final View view = parentView.findViewById(R.id.view_fare_cal_line);
        ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    int yAxisValue = ((percentageOfIncreaseInFare * (view.getBottom() - view.getTop()) / 100) + view.getTop());

                    RelativeLayout relativelayout = parentView.findViewById(R.id.lyt_parent_fare_cal_item);
                    LinearLayout.LayoutParams params = new LinearLayout
                            .LayoutParams(CIRCLE_IMAGE_WIDTH, CIRCLE_IMAGE_WIDTH);
                    ImageView imageview = new ImageView(HomeActivity.this);
                    imageview.setImageResource(R.drawable.circle_with_grey_border);
                    imageview.setLayoutParams(params);
                    imageview.setX(view.getX() - (CIRCLE_IMAGE_WIDTH / 2));
                    imageview.setY(yAxisValue - (CIRCLE_IMAGE_WIDTH / 2));

                    mCircleCoordinates.add(new CoOrdinatePojo(view.getX() - (CIRCLE_IMAGE_WIDTH / 2),
                            yAxisValue - (CIRCLE_IMAGE_WIDTH / 2)));

                    relativelayout.addView(imageview);
                    relativelayout.setTag(childPosition);

                    //Add Fare Text To View
                    TextView fareTextView = new TextView(HomeActivity.this);
                    fareTextView.setId(textViewId);
                    fareTextView.setText((getString(R.string.rupee) + fareCalItem.getFareValue()));
                    fareTextView.setTextColor(Color.parseColor("#b8c2cb"));
                    fareTextView.setTypeface(null, Typeface.BOLD);
                    fareTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
                    fareTextView.setPadding(10,5,10,5);
                    LinearLayout.LayoutParams textViewParams = new LinearLayout
                            .LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    fareTextView.setLayoutParams(textViewParams);
                    fareTextView.setX(view.getX() - (40));
                    fareTextView.setY(yAxisValue - (70));
                    relativelayout.addView(fareTextView);


                    //End of Adding all items
                    if (mCircleCoordinates.size() == mFareCalItems.size()) {
                        setBlueBackground(mCurrentSelectedDatePosition);
                    }

                    relativelayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCurrentSelectedDatePosition = Integer.valueOf(String.valueOf(v.getTag().toString()));
                            setBlueBackground(mCurrentSelectedDatePosition);
                            mPreviousSelectedDatePosition = mCurrentSelectedDatePosition;
                        }
                    });
                }
            });
        }
    }

    private int getPercentageOfIncreaseInFare(int minValue, int maxValue, int fareValue) {
        int range = maxValue - minValue;
        int correctedStartValue = fareValue - minValue;
        return (correctedStartValue * 100) / range;
    }

    private void setBlueBackground(int selectedItemPosition) {
        LinearLayout linearLayout = findViewById(R.id.lyt_parent_linear);

        //Deselect Last selected item
        View prevGraphItem = linearLayout.getChildAt(mPreviousSelectedDatePosition);
        prevGraphItem.findViewById(R.id.view_blue_bg).setVisibility(View.INVISIBLE);
        TextView textView = (TextView) prevGraphItem.findViewById(textViewId);
        if (textView != null) {
            textView.setTextColor(Color.parseColor("#b8c2cb"));
            textView.setBackgroundColor(Color.TRANSPARENT);
            textView.setPadding(10,5,10,5);
        }


        //Highlight current Item
        View fareGraphItem = linearLayout.getChildAt(mCurrentSelectedDatePosition);
        fareGraphItem.findViewById(R.id.view_blue_bg).setVisibility(View.VISIBLE);
        TextView selectedTextView = (TextView) fareGraphItem.findViewById(textViewId);
        if (selectedTextView != null) {
            selectedTextView.setTextColor(Color.parseColor("#FFFFFF"));
            selectedTextView.setBackgroundResource(R.drawable.rectangle_blue);
            selectedTextView.setPadding(10,5,10,5);
        }

        //Highlight text color

    }

    private void prepareFareCalData() {
        mFareCalItems.add(new FareCalItem(3300, "Mon", "11"));
        mFareCalItems.add(new FareCalItem(3200, "Tue", "12"));
        mFareCalItems.add(new FareCalItem(3500, "Wed", "13"));
        mFareCalItems.add(new FareCalItem(3600, "Thu", "14"));
        mFareCalItems.add(new FareCalItem(3400, "Fri", "15"));
        mFareCalItems.add(new FareCalItem(3200, "Sat", "16"));
        mFareCalItems.add(new FareCalItem(3700, "Sun", "17"));
        mFareCalItems.add(new FareCalItem(3600, "Mon", "18"));
        mFareCalItems.add(new FareCalItem(3300, "Tue", "19"));
        mFareCalItems.add(new FareCalItem(3200, "Wed", "20"));
    }

}
